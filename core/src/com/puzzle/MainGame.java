package com.puzzle;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.puzzle.screens.SplashScreen;

public class MainGame extends Game
{
	public static final String TITLE= "Slidex";
	public static final float VERSION= 0.1f;
	//La rotacion se define en el manifest


	public SpriteBatch batch;

	public BitmapFont font;
	@Override
	public void create () {

		batch= new SpriteBatch();
		font= new BitmapFont();

		this.setScreen(new SplashScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose () {
        batch.dispose();
        this.getScreen().dispose();
    }
}
package com.puzzle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.compression.lzma.Base;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.puzzle.BaseScreen;
import com.puzzle.MainGame;

public class SplashScreen extends BaseScreen {


    private Stage stage;

    private Image splash;
    private Image fondo;

    public SplashScreen(MainGame game) {
        super(game);
        this.stage=super.stage;


        Texture fondotex= new Texture(Gdx.files.internal("img/fondo.png"));
        fondo = new Image(fondotex);
        fondo.setSize(stage.getWidth() , stage.getHeight());
        fondo.setPosition(0,0);


        Texture splashtex= new Texture(Gdx.files.internal("img/recurso2.png"));
        splash= new Image(splashtex);
        splash.setSize(120,120);
        splash.setPosition(stage.getWidth()/2 - splash.getWidth()/2 , stage.getHeight());


    }

    @Override
    public void show() {

        stage.addActor(fondo);
        stage.addActor(splash);



        //Remember add "STATIC" when are import "Actions".
        //At the end add ".*", before the ";"

        splash.addAction(sequence(alpha(0f), scaleTo(.3f,.3f),
                parallel(fadeIn(4f, Interpolation.pow2),
                        scaleTo(1.5f,1.5f,3f , Interpolation.pow5),
                        moveTo(stage.getWidth()/2-90, stage.getHeight()/2 - 90,3f, Interpolation.swing)),
                delay(1.5f), fadeOut(1.25f)));





        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f,1f,1f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update(delta);

        stage.draw();
        stage.act();

        game.batch.begin();
        game.font.draw(game.batch, "Splashscreen! holi", 120, 120);
        game.batch.end();
    }

    public void update(float delta){
        stage.act(delta);
    }
    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    stage.dispose();
    }
}

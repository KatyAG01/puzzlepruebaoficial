package com.puzzle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.puzzle.BaseScreen;
import com.puzzle.MainGame;

public class LoadingScreen extends BaseScreen {
    private Stage stage;
    private Skin skin;
    private Label loading;

    public LoadingScreen(MainGame game) {
        super(game);
        stage=super.stage;

        //add actors using stage
    }

    @Override
    public void render(float delta)
    {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }
    @Override
    public void dispose() {
        skin.dispose();
        stage.dispose();


    }

    @Override
    public void resize(int width, int height)
    {
        stage.getViewport().update(width,height);
    }
}
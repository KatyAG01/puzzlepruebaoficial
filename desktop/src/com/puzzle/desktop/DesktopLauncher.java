package com.puzzle.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.puzzle.MainGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title =MainGame.TITLE +" v" + MainGame.VERSION;
		config.backgroundFPS = 60;
		config.foregroundFPS =60 ;
		config.resizable= false;

		new LwjglApplication(new MainGame(), config);

	}
}
